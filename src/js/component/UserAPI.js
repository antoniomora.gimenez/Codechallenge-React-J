import axios from 'axios';

const API_URL = 'https://randomuser.me/api?results=10&noinfo';

const fetchUsers = async () => {
  try {
    const response = await axios.get(API_URL);
    return response.data.results;
  } catch (error) {
    throw error;
  }
};

export default {
  fetchUsers,
};
