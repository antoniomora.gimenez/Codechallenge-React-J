// src/UserList.js
import React, { useState, useEffect } from 'react';
import UserAPI from '../UserAPI';
import UserDetail from '../UserDetail/UserDetail'; 
import "./UserList.css";

function UserList() {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null); // Nuevo estado para el usuario seleccionado

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await UserAPI.fetchUsers();
        setUsers(data);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchData();
  }, []);

  // Función para mostrar el detalle de un usuario cuando se hace clic en su nombre
  const handleUserClick = (user) => {
    setSelectedUser(user);
  };

  return (
    <div className="user-list"> 
      <h2>Lista de usuarios</h2>
      <ul>
        {users.map((user, index) => (
          <li key={index} onClick={() => handleUserClick(user)} className="user-list-item">
            {user.name.first} {user.name.last}
          </li>
        ))}
      </ul>
      {/* Mostrar el detalle del usuario seleccionado */}
      {selectedUser && <UserDetail user={selectedUser} />}
    </div>
  );
}

export default UserList;