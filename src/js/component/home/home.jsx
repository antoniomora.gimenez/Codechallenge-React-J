import React from "react";
import UserList from "../UserList/UserList";
import "./home.css"; 
import { FaUserNinja } from "react-icons/fa";

const Home = () => {
  return (
    <div className="Home"> 
      <header className="Home-header">
        <h1>
          <FaUserNinja className="icon" /> 
          <span className="text">Ninja Talent App</span> 
        </h1>
        <UserList />
      </header>
    </div>
  );
};

export default Home;
