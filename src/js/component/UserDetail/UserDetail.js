
import React from 'react';
import './UserDetail.css';

function UserDetail({ user }) {
  return (
    <div className="user-detail">
      <h2>Detalles del usuario</h2>
      <div>
        <img src={user.picture.large} alt={`${user.name.first} ${user.name.last}`} />
      </div>
      <div>
        <strong>Name:</strong> {user.name.title} {user.name.first} {user.name.last}
      </div>
      <div>
        <strong>Gender:</strong> {user.gender}
      </div>
      <div>
        <strong>Location:</strong> {user.location.street.number} {user.location.street.name}, {user.location.city}, {user.location.state}, {user.location.country}, {user.location.postcode}
      </div>
      <div>
        <strong>Coordinates:</strong> Latitude: {user.location.coordinates.latitude}, Longitude: {user.location.coordinates.longitude}
      </div>
      <div>
        <strong>Timezone:</strong> {user.location.timezone.description} (GMT {user.location.timezone.offset})
      </div>
      <div>
        <strong>Email:</strong> {user.email}
      </div>
    </div>
  );
}

export default UserDetail;
